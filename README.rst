.. SPDX-FileCopyrightText: 2021-2024 Helmholtz-Zentrum hereon GmbH
..
.. SPDX-License-Identifier: CC-BY-4.0

Graphical User Interface for the psyplot package
================================================

.. start-badges

|CI|
|Code coverage|
|Latest Release|
|PyPI version|
|Code style: black|
|Imports: isort|
|PEP8|
|REUSE status|


.. end-badges

Welcome! This package enhances the interactive visualization framework
psyplot_ with a graphical user interface using PyQt_. See the homepage of the
main project on examples on the possibilities with psyplot_.

You can see the full documentation under
`psyplot.github.io/psyplot-gui/ <http://psyplot.github.io/psyplot-gui>`__

.. _PyQt: https://riverbankcomputing.com/software/pyqt/intro
.. _psyplot: http://psyplot.github.io/


Copyright
---------
Copyright © 2024 Helmholtz-Zentrum Hereon, 2020-2021 Helmholtz-Zentrum
Geesthacht, 2016-2024 University of Lausanne

Code files in this repository are licensed under the
LGPL-3.0-only, if not stated otherwise in the file.

Documentation files in this repository are licensed under CC-BY-4.0, if not
stated otherwise in the file.

Supplementary and configuration files in this repository are licensed
under CC0-1.0, if not stated otherwise
in the file.

Please check the header of the individual files for more detailed
information.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License version 3.0 as
published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU LGPL-3.0 license for more details.

You should have received a copy of the GNU LGPL-3.0 license
along with this program.  If not, see https://www.gnu.org/licenses/.

.. |CI| image:: https://codebase.helmholtz.cloud/psyplot/psyplot-gui/badges/master/pipeline.svg
   :target: https://codebase.helmholtz.cloud/psyplot/psyplot-gui/-/pipelines?page=1&scope=all&ref=master
.. |Code coverage| image:: https://codebase.helmholtz.cloud/psyplot/psyplot-gui/badges/master/coverage.svg
   :target: https://codebase.helmholtz.cloud/psyplot/psyplot-gui/-/graphs/master/charts
.. |Latest Release| image:: https://codebase.helmholtz.cloud/psyplot/psyplot-gui/-/badges/release.svg
   :target: https://codebase.helmholtz.cloud/psyplot/psyplot-gui
.. |PyPI version| image:: https://img.shields.io/pypi/v/psyplot-gui.svg
   :target: https://pypi.python.org/pypi/psyplot-gui/
.. |Code style: black| image:: https://img.shields.io/badge/code%20style-black-000000.svg
   :target: https://github.com/psf/black
.. |Imports: isort| image:: https://img.shields.io/badge/%20imports-isort-%231674b1?style=flat&labelColor=ef8336
   :target: https://pycqa.github.io/isort/
.. |PEP8| image:: https://img.shields.io/badge/code%20style-pep8-orange.svg
   :target: https://www.python.org/dev/peps/pep-0008/
.. |REUSE status| image:: https://api.reuse.software/badge/codebase.helmholtz.cloud/psyplot/psyplot-gui
   :target: https://api.reuse.software/info/codebase.helmholtz.cloud/psyplot/psyplot-gui
